require 'spec_helper'
require 'launchy'

feature "the signup feature" do

  it "has a new user page" do
    visit new_user_url
    expect(page).to have_content "Sign Up"
  end

  feature "signing up a user" do
    before(:each) do
     sign_up
    end

    it "shows username on the homepage after signup" do
      expect(page).to have_content "First User"
    end

  end

end

feature "logging in" do
  before(:each) do
    sign_up
    click_on("Sign Out")
    sign_in

  end

  it "shows username on the homepage after login" do
    expect(page).to have_content "First User"
  end

  it "has a log out button" do
    click_button("Sign Out")
    expect(page).to have_content "Sign In"
  end
end

feature "logging out" do
  before(:each) do
    sign_up
    click_on "Sign Out"
  end

  it "begins with logged out state" do
    expect(page).to have_content "Sign In"
  end

  it "doesn't show username on the homepage after logout" do
    expect(page).not_to have_content("First User")
  end
end