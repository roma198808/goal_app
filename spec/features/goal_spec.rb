require 'spec_helper'
require 'launchy'

feature "new goal form" do
  before(:each) do
    sign_up
  end
  it "ensures a user is signed in" do
    click_button("Sign Out")
    visit new_goal_url
    expect(page).to have_content "Sign In"
  end

  it "has a new goal form" do
    visit new_goal_url
    expect(page).to have_content "Create Goal"
  end

  it "can create a new goal" do
    create_goal
    expect(page).to have_content("First Goal")
  end
end

feature "show the goal" do
  before(:each) do
    sign_in
  end

  it "shows each goal on a separate page" do
    create_goal
    visit "/goals"
    click_on("First Goal")
    expect(page).to have_content("First Goal")
  end

  it "lets you edit the goal" do
    create_goal
    click_on("First Goal")
    click_on("Edit Goal")
    expect(page).to have_content("Edit Goal")
  end

  it "lets you delete the goal" do
    create_goal
    click_on("First Goal")
    click_on("Delete Goal")
    expect(page).not_to have_content("First Goal")
  end

  it "should show the progress of the goal on the index page" do
    create_goal
    expect(page).to have_content("Not Completed")
  end
end
