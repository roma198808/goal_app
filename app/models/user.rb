class User < ActiveRecord::Base
  attr_accessible :password, :token, :username, :admin
  attr_reader :password

  validates :username, presence: true
  before_validation :reset_session_token

  has_many :goals

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def reset_session_token
    self.token = SecureRandom.urlsafe_base64(16)
  end

  def reset_session_token!
    self.token = SecureRandom.urlsafe_base64(16)
    self.save!
    self.token
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.find_by_credentials(username, password)
    user = User.find_by_username(username)
    return nil if user.nil?

    user.is_password?(password) ? user : nil
  end

end
