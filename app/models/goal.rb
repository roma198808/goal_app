class Goal < ActiveRecord::Base
   attr_accessible :goal_name, :visibility, :progress, :user_id

   validates :goal_name, presence: true
   validates :visibility, :inclusion => {:in => [true, false]}

   belongs_to :user
end
