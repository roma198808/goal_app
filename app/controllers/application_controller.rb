class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user

  def current_user
    return nil unless session[:token]
    @current_user ||= User.find_by_token(session[:token])
  end

  def signin(user)
    @current_user = user
    session[:token] = user.reset_session_token!
  end

  def signout
    session[:token] = nil
  end

  def rcu
    redirect_to new_session_url unless current_user
  end
end
