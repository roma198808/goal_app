class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.string :goal_name
      t.boolean :visibility
      t.boolean :progress, :default => false
      t.integer :user_id
      t.timestamps
    end
    add_index :goals, :user_id
  end
end
